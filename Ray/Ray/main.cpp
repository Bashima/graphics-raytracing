//
//  main.cpp
//  RayTracing
//
//  Created by Bashima Islam on 12/9/15.
//  Copyright © 2015 Bashima Islam. All rights reserved.
//

#include <iostream>
#include <cstring>
#include <vector>
#include <GLUT/GLUT.h>
#include <OpenGL/OpenGL.h>
#include "bitmap_image.h"
#include "vector.h"

#define BLACK 0, 0, 0
#define BLUE 0, 0, 1
#define WINDOWSIZE pixels
#define HALFWINDOWSIZE WINDOWSIZE/2
#define pixelFactor  .002

using namespace std;

#define MAX 999999
#define COUNT 3

int pixels;
int recDepth;
double cameraAngle;			//in radian
double cameraAngleDelta;
double height,r;
double rectAngle;	//in degree
bool canDrawGrid;
double a[COUNT],b[COUNT],c[COUNT];
GLUquadric* IDquadric = gluNewQuadric() ;
GLfloat diffuseDir[4];
GLfloat lmodel_ambient[4];

Vector eye, look, up, n, u, v;

class Color {
public:
    double rgb[COUNT];
    Color() {};
    Color(double r, double g, double b)
    {
        rgb[0] = r;
        rgb[1] = g;
        rgb[2] = b;
    }
    void set(double r, double g, double b)
    {
        rgb[0] = r;
        rgb[1] = g;
        rgb[2] = b;
    }
};
Color colorMatrix[1024][1024];

class Ray {
public:
    Vector origin;
    Vector direction;
};
Ray rays[1024][1024];

class checkerBoard{
public:
    Color color;
    double ambCoeff;
    double difCoeff;
    double refCoeff;
    double specCoeff;
    double specExp;
    Color colorTwo;
    checkerBoard(){}
};
vector<checkerBoard> checkerBoardVector;

class Sphere {
public:
    Color color;
    double ambCoeff;
    double difCoeff;
    double refCoeff;
    double specCoeff;
    double specExp;
    double center[COUNT];
    double radius;
    Sphere(){}
};
vector<Sphere> sphereVector;

class Cylinder {
public:
    Color color;
    double ambCoeff;
    double difCoeff;
    double refCoeff;
    double specCoeff;
    double specExp;
    double center_x;
    double center_z;
    double radius;
    double min_y;
    double max_y;
    Cylinder(){}
};
vector<Cylinder> cylinderVector;

class Triangle  {
public:
    Color color;
    double ambCoeff;
    double difCoeff;
    double refCoeff;
    double specCoeff;
    double specExp;
    double a[COUNT],b[COUNT],c[COUNT];
    double refractive_idx;
    Triangle(){}
};
vector<Triangle> triangleVector;

class LightSource
{
public:
    double x, y, z;
};
vector<LightSource> lightSources;

void readFile() {
    static const char filename[] = "/Users/bashimaislam/Desktop/1005095/Ray/Ray/Spec.txt";


    ifstream input(filename);
    if (!input.is_open()){
        cout<<"error opening file\n";
        return;
    }
    string str;
    input>>str;
    input>>recDepth;
    //    cout<<str<<recDepth;
    input>>str;
    input>>pixels;
    while(!input.eof()){ {
        input>>str;
        if(str.compare("objStart")==0){
            input>>str;
            if(str.compare("CHECKERBOARD")==0){
                input>>str;
                checkerBoard newCheckerBoard;

                input>>newCheckerBoard.color.rgb[0]>>newCheckerBoard.color.rgb[1]>>newCheckerBoard.color.rgb[2];

                input>>str;
                input>>newCheckerBoard.colorTwo.rgb[0]>>newCheckerBoard.colorTwo.rgb[1]>>newCheckerBoard.colorTwo.rgb[2];

                input>>str;
                input>>newCheckerBoard.ambCoeff;
                input>>str;
                input>>newCheckerBoard.difCoeff;
                input>>str;
                input>>newCheckerBoard.refCoeff;
                input>>str;
                input>>newCheckerBoard.specCoeff;
                input>>str;
                input>>newCheckerBoard.specExp;
                checkerBoardVector.push_back(newCheckerBoard);
                input>>str;
            }

            else if(str=="SPHERE") {
                Sphere new_sphere;
                input>>str;
                for(int i=0; i<COUNT; i++){
                    input>>new_sphere.center[i];
                }
                input>>str;
                input>>new_sphere.radius;

                input>>str;
                input>>new_sphere.color.rgb[0]>>new_sphere.color.rgb[1]>>new_sphere.color.rgb[2];

                input>>str;
                input>>new_sphere.ambCoeff;
                input>>str;
                input>>new_sphere.difCoeff;
                input>>str;
                input>>new_sphere.refCoeff;
                input>>str;
                input>>new_sphere.specCoeff;
                input>>str;
                input>>new_sphere.specExp;
                sphereVector.push_back(new_sphere);
                input>>str;
            }
            else if(str=="CYLINDER") {
                Cylinder new_cylinder;
                input>>str;
                input>>new_cylinder.center_x;
                input>>str;
                input>>new_cylinder.center_z;
                input>>str;
                input>>new_cylinder.radius;
                input>>str;
                input>>new_cylinder.min_y;
                input>>str;
                input>>new_cylinder.max_y;

                input>>str;
                input>>new_cylinder.color.rgb[0]>>new_cylinder.color.rgb[1]>>new_cylinder.color.rgb[2];

                input>>str;
                input>>new_cylinder.ambCoeff;
                input>>str;
                input>>new_cylinder.difCoeff;
                input>>str;
                input>>new_cylinder.refCoeff;
                input>>str;
                input>>new_cylinder.specCoeff;
                input>>str;
                input>>new_cylinder.specExp;
                cylinderVector.push_back(new_cylinder);
                input>>str;
            }
            else if(str=="TRIANGLE") {
                Triangle new_triangle;
                input>>str;
                input>>new_triangle.a[0];
                input>>new_triangle.a[1];
                input>>new_triangle.a[2];
                input>>str;
                input>>new_triangle.b[0];
                input>>new_triangle.b[1];
                input>>new_triangle.b[2];
                input>>str;
                input>>new_triangle.c[0];
                input>>new_triangle.c[1];
                input>>new_triangle.c[2];

                input>>str;
                input>>new_triangle.color.rgb[0]>>new_triangle.color.rgb[1]>>new_triangle.color.rgb[2];

                input>>str;
                input>>new_triangle.ambCoeff;
                input>>str;
                input>>new_triangle.difCoeff;
                input>>str;
                input>>new_triangle.refCoeff;
                input>>str;
                input>>new_triangle.specCoeff;
                input>>str;
                input>>new_triangle.specExp;
                input>>str;
                input>>new_triangle.refractive_idx;
                triangleVector.push_back(new_triangle);
                input>>str;
            }
        }
        else {
            LightSource l_source;
            input>>l_source.x>>l_source.y>>l_source.z;
            lightSources.push_back(l_source);
        }   }
    }
}

double intersectSphere (Sphere sphere, Ray ray) {
    double value1, value2;
    double a = ray.direction * ray.direction;
    Vector center(sphere.center[0], sphere.center[1], sphere.center[2]);

    Vector pc = ray.origin - center;

    double b = pc * ray.direction;
    double c = pc * pc;

    c = c - sphere.radius * sphere.radius;

    double check1 = (b * b) - (a * c);

    if(check1<0) {
        return 100000;
    }

    else {
        double determinant = sqrt(check1);

        value1 = (- b + determinant) / a;
        value2 = (- b - determinant) / a;

    }

    if(value1<0) {
        if (value2<0) {
            return 100000;
        }
        else {
            return value2;
        }

    }
    else{
        if (value2<0) {
            return value1;
        }
        else {
            return fmin(value1, value2);
        }
    }
}

double get_intersection_with_triangle (Triangle tri, Ray ray){
    Vector a(tri.a[0], tri.a[1], tri.a[2]);
    Vector b(tri.b[0], tri.b[1], tri.b[2]);
    Vector c(tri.c[0], tri.c[1], tri.c[2]);
    Vector ab = b - a;
    Vector ac= c - a;
    Vector n = cross(ab, ac);
    Vector d = ray.direction;
    Vector o = ray.origin;
    double tt = d * n;
    if(tt==0) return MAX;
    double t1 = n * a - n * o;
    double val = t1/tt;
    if(val<0) return MAX;
    Vector p (o.x+ val*d.x, o.y+ val*d.y, o.z+ val*d.z);
    double temp1,temp2,temp3;
    Vector ap = p - a;
    Vector temp_v = cross(ab, ap);
    temp1 = temp_v * n;
    Vector bc = c - b;
    Vector bp = p - b;
    temp_v = cross(bc, bp);
    temp2 = temp_v * n;
    Vector ca = a - c;
    Vector cp = p - c;
    temp_v = cross(ca, cp);
    temp3 = temp_v * v;
    if(temp1<=0 && temp2<=0 && temp3<=0) return val;
    else if(temp1>=0 && temp2>=0 && temp3>=0) return val;
    return MAX;

}

double intersectCheckerBoard( Ray ray ){

    double t = -ray.origin.z / ray.direction.z;
    if (t<0)return MAX;

    if ( t>=0 ){
        return t;
    }else{
        return MAX;
    }
    
}


double intersectPlane(Vector n, Vector v1, Ray ray){
    Vector d(ray.direction.x, ray.direction.y, ray.direction.z);
    Vector o(ray.origin.x, ray.origin.y, ray.origin.z);
    double tt = d * n;
    if (tt<.00002) {
        return MAX;
    }
    double t1 = n * v1 - n * o;
    double val = t1/tt;
    if(val<0) return MAX;
    return val;
}

double intersectCylinder (Cylinder cyl, Ray ray){
    double a = (ray.direction.x * ray.direction.x) + (ray.direction.z * ray.direction.z);
    double c = (ray.origin.x * ray.origin.x) + (cyl.center_x * cyl.center_x) + (ray.origin.z * ray.origin.z) + (cyl.center_z * cyl.center_z) - (2 * ray.origin.x * cyl.center_x) - (2 * ray.origin.z * cyl.center_z) - (cyl.radius * cyl.radius);
    double b = 2 * (ray.direction.x * (ray.origin.x - cyl.center_x) + ray.direction.z * (ray.origin.z - cyl.center_z));
    double d = (b*b) - (4*a*c);
    if(d<0) return MAX;
    d = sqrt(d);
    double root1 = (-b + d) / (2 * a);
    double root2 = (-b - d) / (2 * a);
    double root;
    if (root1<0 && root2<0) {
        return MAX;
    }
    else if (root1 < 0) root = root2;
    else if (root2 < 0) root = root1;
    else if (root1 < root2) root = root1;
    else root = root2;

    double y = ray.origin.y + ray.direction.y * root;

    //upper lid
    Vector n1 = Vector(cyl.center_x, cyl.max_y, cyl.center_z) - Vector(cyl.center_x - 5, cyl.max_y, cyl.center_z);
    Vector n2 = Vector(cyl.center_x, cyl.max_y, cyl.center_z) - Vector(cyl.center_x, cyl.max_y, cyl.center_z + 5);
    Vector n = cross(n1, n2);
    double plane_intersection = intersectPlane(n, Vector(cyl.center_x, cyl.max_y, cyl.center_z), ray);
    double x = ray.origin.x + ray.direction.x * plane_intersection;
    double z = ray.origin.z + ray.direction.z * plane_intersection;
    double temp = (x - cyl.center_x) * (x - cyl.center_x) + (y - cyl.max_y) * (y - cyl.max_y) + (z - cyl.center_z) * (z - cyl.center_z) - (cyl.radius * cyl.radius);
    if(temp<=0) return root;

    //lower lid
    n1 = Vector(cyl.center_x, cyl.min_y, cyl.center_z) - Vector(cyl.center_x - 5,cyl.min_y,cyl.center_z);
    n2 = Vector(cyl.center_x, cyl.min_y, cyl.center_z) - Vector(cyl.center_x, cyl.min_y, cyl.center_z + 5);
    n = cross(n1, n2);;
    plane_intersection = intersectPlane(n, Vector(cyl.center_x, cyl.min_y, cyl.center_z), ray);
    x = ray.origin.x + ray.direction.x * plane_intersection;
    z = ray.origin.z + ray.direction.z * plane_intersection;
    temp = (x - cyl.center_x) * (x - cyl.center_x) + (y - cyl.min_y) * (y - cyl.min_y) + (z - cyl.center_z) * (z - cyl.center_z) - (cyl.radius* cyl.radius);
    if(temp<=0) return root;
    if(y>=cyl.min_y && y<= cyl.max_y) return root;
    else return MAX;
}

Color getPixelColor(int depth, Ray ray)
{
    double min = MAX;
    Color color(0, 0, 0);
    int id = -1;
    int x, z, y;
    double cmin=0;
    int val=intersectCheckerBoard(ray);
    if(val<min){
        x=(int)(ray.origin.x+(val*ray.direction.x));
        y=(int)(ray.origin.y+(val*ray.direction.y));
        z=(int)(ray.origin.z+(val*ray.direction.z));
        if(x<=100 && x>=-100 && z<=100 && z>=-100 && y>=0){
            min=val;
            id =100;
        }
    }
    cmin=min;
    min=99999;
    int id_s = -1;
    for(int i=0; i<sphereVector.size(); i++){
        double val = intersectSphere(sphereVector[i], ray);
        if(val<=min){
            min = val;
            id_s = i;
            if (cmin<=min) {
                id_s = -1;
            }
        }
    }

    int id_t = -1;
    for(int i=0; i<triangleVector.size(); i++){
        double val = get_intersection_with_triangle(triangleVector[i], ray);
        if(val<=min){
            min = val;
            id_t = i;
        }
    }

    int id_c = -1;
    for(int i=0; i<cylinderVector.size(); i++){
        double val = intersectCylinder(cylinderVector[i], ray);
        if(val<=min){
            min = val;
            id_c = i;
        }
    }

    if (id_c>=0) {
        color = cylinderVector[id_c].color;
    }
    else if (id_t>=0) {
        color = triangleVector[id_t].color;
    }
    else if (id_s>=0) {
        color = sphereVector[id_s].color;
    }
    else if (id>=0 && cmin<min) {
        double t = -ray.origin.z / ray.direction.z;
        Vector pos = ray.origin + ray.direction*t;
        int i = (pos.x + 200) / 15;
        int j = (pos.y + 200) / 15;
        			if((i+j)%2==0){
        				for(int i=0;i<COUNT;i++) color.rgb[i]=checkerBoardVector[0].color.rgb[i];
        			}
        			else {
        				for(int i=0;i<COUNT;i++) color.rgb[i]=checkerBoardVector[0].colorTwo.rgb[i];
        			}

    }
    return color;
}



void generateRays()
{
    eye = Vector(a[0], a[1], a[2]);
    look = Vector(b[0], b[1], b[2]);
    n = eye - look;
    n.normalize();
    up = Vector(c[0], c[1], c[2]);
    u = cross(up,n);
    u.normalize();
    v = cross(n,u);
    v.normalize();
    for(int i=0; i<WINDOWSIZE; i++) {
        for(int j=0; j<WINDOWSIZE; j++) {
            rays[i][j].origin = v * ((i-HALFWINDOWSIZE) * pixelFactor) + u * ((j-HALFWINDOWSIZE) * pixelFactor) + eye ;

            Vector newPosition = eye + n;
            rays[i][j].direction = rays[i][j].origin - newPosition;
        }
    }

}
double sqr(double x)
{
    return x*x;
}
double getLength(double x, double y, double z)
{
    return sqrt((x*x)+(y*y)+(z*z));
}
double getLength(double x[COUNT])
{
    return sqrt(sqr(x[0])+sqr(x[1])+sqr(x[2]));
}
double getLength(float x[COUNT])
{
    return sqrt(sqr(x[0])+sqr(x[1])+sqr(x[2]));
}

void walk_fly(int d, bool f)
{
    double da[COUNT];
    // find vector
    if (f) //fly
    {
        for (int i=0; i<COUNT; i++) {
            da[i]=c[i];
        }
    }
    else //walk
    {
        for (int i=0; i<COUNT; i++) {
            da[i]=b[i]-a[i];
        }
    }
    double len= getLength(da);
    //normalize
    for (int i=0; i<COUNT; i++) {
        da[i]=da[i]/len;
    }
    for (int i=0; i<COUNT; i++) {
        da[i] = da[i]*d;
        a[i]=a[i]+da[i];
        b[i]=b[i]+da[i];
    }
}

void starf(int d)
{
    double da[COUNT],dap[COUNT];
    for (int i=0; i<COUNT; i++) {
        dap[i]=b[i]-a[i];
    }
    da[0]=dap[1]*c[2]-dap[2]*c[1];
    da[1]=dap[2]*c[0]-dap[0]*c[2];
    da[2]=dap[0]*c[1]-dap[1]*c[0];
    double len=getLength(da);
    //normalize
    for (int i=0; i<COUNT; i++) {
        da[i]=da[i]/len;
    }
    for (int i=0; i<COUNT; i++) {
        da[i] = da[i]*d;
        a[i]=a[i]+da[i];
        b[i]=b[i]+da[i];
    }
}

void yaw(double d)
{
    double da[COUNT],dd[COUNT];
    for (int i=0; i<COUNT; i++) {
        dd[i]=b[i]-a[i];
    }
    double len= getLength(c);
    for (int i=0; i<COUNT; i++) {
        da[i]=c[i]/len;
    }
    double t=dd[0]*da[0]+dd[1]*da[1]+dd[2]*da[2];
    b[0]=a[0]+cos((double)d)*dd[0]+(1-cos((double)d))*t*da[0]+sin((double)d)*(da[1]*dd[2]-da[2]*dd[1]);
    b[1]=a[1]+cos((double)d)*dd[1]+(1-cos((double)d))*t*da[1]+sin((double)d)*(da[2]*dd[0]-da[0]*dd[2]);
    b[2]=a[2]+cos((double)d)*dd[2]+(1-cos((double)d))*t*da[2]+sin((double)d)*(da[0]*dd[1]-da[1]*dd[0]);

}
void roll(double d)
{
    double da[COUNT];
    for (int i=0; i<COUNT; i++) {
        da[i]=b[i]-a[i];
    }
    double len=getLength(da);
    //normalize
    for (int i=0; i<COUNT; i++) {
        da[i]=da[i]/len;
    }
    double t=c[0]*da[0]+c[1]*da[1]+c[2]*da[2];
    c[0]=cos((double)d)*c[0]+(1-cos((double)d))*t*da[0]+sin((double)d)*(da[1]*c[2]-da[2]*c[1]);
    c[1]=cos((double)d)*c[1]+(1-cos((double)d))*t*da[1]+sin((double)d)*(da[2]*c[0]-da[0]*c[2]);
    c[2]=cos((double)d)*c[2]+(1-cos((double)d))*t*da[2]+sin((double)d)*(da[0]*c[1]-da[1]*c[0]);
}

void pitch(double d)
{
    double da[COUNT],t[COUNT];
    for (int i=0; i<COUNT; i++) {
        t[i]=b[i]-a[i];
    }
    da[0]=(t[1]*c[2]-t[2]*c[1]);
    da[1]=t[2]*c[0]-t[0]*c[2];
    da[2]=t[0]*c[1]-t[1]*c[0];
    double len= getLength(da);
    //normalize
    for (int i=0; i<COUNT; i++) {
        da[i]=da[i]/len;
    }
    double dd[COUNT];
    for (int i=0; i<COUNT; i++) {
        dd[i]=c[i];
    }
    double td=dd[0]*da[0]+dd[1]*da[1]+dd[2]*da[2];
    c[0]=cos((double)d)*dd[0]+(1-cos((double)d))*td*da[0]+sin((double)d)*(da[1]*dd[2]-da[2]*dd[1]);
    c[1]=cos((double)d)*dd[1]+(1-cos((double)d))*td*da[1]+sin((double)d)*(da[2]*dd[0]-da[0]*dd[2]);
    c[2]=cos((double)d)*dd[2]+(1-cos((double)d))*td*da[2]+sin((double)d)*(da[0]*dd[1]-da[1]*dd[0]);
    for (int i=0; i<COUNT; i++) {
        dd[i]=t[i];
    }
    b[0]=a[0]+cos((double)d)*dd[0]+(1-cos((double)d))*td*da[0]+sin((double)d)*(da[1]*dd[2]-da[2]*dd[1]);
    b[1]=a[1]+cos((double)d)*dd[1]+(1-cos((double)d))*td*da[1]+sin((double)d)*(da[2]*dd[0]-da[0]*dd[2]);
    b[2]=a[2]+cos((double)d)*dd[2]+(1-cos((double)d))*td*da[2]+sin((double)d)*(da[0]*dd[1]-da[1]*dd[0]);
}

void render()
{
    cout<<"started";
    bitmap_image image(WINDOWSIZE, WINDOWSIZE);
    image.set_all_channels(0,0,0);

    generateRays();
    for(int i=0; i<WINDOWSIZE; i++)
    {
        for(int j=0; j<WINDOWSIZE; j++)
        {
            Color color = getPixelColor(0, rays[i][j]);
            colorMatrix[i][j] = color;
        }
    }

    for (int i=0; i<WINDOWSIZE; i++)
    {
        for (int j=0; j<WINDOWSIZE; j++)
        {
            image.set_pixel(j, WINDOWSIZE-i, colorMatrix[i][j].rgb[0]*255, colorMatrix[i][j].rgb[1]*255, colorMatrix[i][j].rgb[2]*255);
        }
    }

    image.save_image("/Users/bashimaislam/Desktop/1005095/Ray/out.bmp");
    cout<<"ended";
}
void spot()
{

    GLfloat spotLightPosition[] = {0, -1000, 500, 1};
    GLfloat cutOffAngle[] = {45.0};
    GLfloat fallOfExponent[] = {0.3};
    glLightfv(GL_LIGHT2, GL_POSITION, spotLightPosition);
    glLightfv(GL_LIGHT2, GL_SPOT_CUTOFF, cutOffAngle);
    GLfloat spotDirection[] = {0, 1, -1};
    glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, spotDirection);
    glLightfv(GL_LIGHT2, GL_SPOT_EXPONENT, fallOfExponent);

    // draw spot light
    glPushMatrix();
    glColor3ub(BLUE);
    glTranslatef (spotLightPosition[0], spotLightPosition[1], spotLightPosition[2]);
    glutSolidSphere(150, 36, 36);
    glPopMatrix();
}

void col(float r, float g, float b)
{
    GLfloat mat_ambient[] = { r, g, b, 1.0 };
    glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
    GLfloat mat_diffuse[] = { r, g, b, 1.0 };
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
}
void display(){

    //clear the display
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(BLACK, 0);	//color black
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /********************
     / set-up camera here
     ********************/
    //load the correct matrix -- MODEL-VIEW matrix
    glMatrixMode(GL_MODELVIEW);

    //initialize the matrix
    glLoadIdentity();

    //now give three info
    //1. where is the camera (viewer)?
    //2. where is the camera is looking?
    //3. Which direction is the camera's UP direction?

    //instead of CONSTANT information, we will define a circular path.
    //	gluLookAt(-30,-30,50,	0,0,0,	0,0,1);

    //	gluLookAt(r*cos(cameraAngle), r*sin(cameraAngle), height,		0,0,0,		0,0,1);//150 is radius
    gluLookAt(a[0],a[1],a[2],	b[0],b[1],b[2],	c[0],c[1],c[2]);
    //NOTE: the camera still CONSTANTLY looks at the center
    // cameraAngle is in RADIAN, since you are using inside COS and SIN

    //
    //	//again select MODEL-VIEW
    //	glMatrixMode(GL_MODELVIEW);
    //
    //	/****************************
    //	 / Add your objects from here
    //	 ****************************/
    //	//add objects

    if (checkerBoardVector.size()) {
        for (int idx=0; idx<checkerBoardVector.size(); idx++) {
            glPushMatrix(); {
                glTranslatef(-100, 0, 0);
                for (int i=0; i<50 ; i++) {
                    for (int j=0; j<50; j++) {
                        if((i + j)%2 == 0) {
                            glColor3f(checkerBoardVector[idx].color.rgb[0], checkerBoardVector[idx].color.rgb[1], checkerBoardVector[idx].color.rgb[2]);
                            col(checkerBoardVector[idx].color.rgb[0], checkerBoardVector[idx].color.rgb[1], checkerBoardVector[idx].color.rgb[2]);
                        }
                        else {
                            glColor3f(checkerBoardVector[idx].colorTwo.rgb[0], checkerBoardVector[idx].colorTwo.rgb[1], checkerBoardVector[idx].colorTwo.rgb[2]);
                            col(checkerBoardVector[idx].colorTwo.rgb[0], checkerBoardVector[idx].colorTwo.rgb[1], checkerBoardVector[idx].colorTwo.rgb[2]);
                        }
                        
                        glRecti(i*5, j*5, (i+1)*5, (j+1)*5);    // draw the rectangle
                    }
                }
            } glPopMatrix();
        }
    }

    if (cylinderVector.size()) {
        for (int idx=0; idx<cylinderVector.size(); idx++) {
            glPushMatrix(); {

                glRotatef(90, 1, 0, 0);
                glColor3f(cylinderVector[idx].color.rgb[0], cylinderVector[idx].color.rgb[1], cylinderVector[idx].color.rgb[2]);
                col(cylinderVector[idx].color.rgb[0], cylinderVector[idx].color.rgb[1], cylinderVector[idx].color.rgb[2]);
                glTranslatef(cylinderVector[idx].center_x, cylinderVector[idx].center_z, -cylinderVector[idx].max_y);

                gluCylinder(IDquadric, cylinderVector[idx].radius, cylinderVector[idx].radius, cylinderVector[idx].max_y - cylinderVector[idx].min_y, 32, 32);
            } glPopMatrix();
        }
    }

    if (triangleVector.size()) {
        for (int idx=0; idx<triangleVector.size(); idx++) {
            glPushMatrix(); {
                glBegin(GL_TRIANGLES);
                glColor3f(triangleVector[idx].color.rgb[0], triangleVector[idx].color.rgb[1], triangleVector[idx].color.rgb[2]);
                col(triangleVector[idx].color.rgb[0], triangleVector[idx].color.rgb[1], triangleVector[idx].color.rgb[2]);
                glVertex3f(triangleVector[idx].a[0], triangleVector[idx].a[1], triangleVector[idx].a[2]);
                glVertex3f(triangleVector[idx].b[0], triangleVector[idx].b[1], triangleVector[idx].b[2]);
                glVertex3f(triangleVector[idx].c[0], triangleVector[idx].c[1], triangleVector[idx].c[2]);
                glEnd();
            } glPopMatrix();
        }
    }

    if (sphereVector.size()) {
        for (int idx=0; idx<sphereVector.size(); idx++) {
            glPushMatrix();
            {
                glColor3f(sphereVector[idx].color.rgb[0], sphereVector[idx].color.rgb[1], sphereVector[idx].color.rgb[2]);
                col(sphereVector[idx].color.rgb[0], sphereVector[idx].color.rgb[1], sphereVector[idx].color.rgb[2]);
                glTranslatef(sphereVector[idx].center[0], sphereVector[idx].center[1], sphereVector[idx].center[2]);
                glutSolidSphere(sphereVector[idx].radius, 32, 32);
            }
            glPopMatrix();
        }
    }
    spot();
    // draw the two AXES
    glColor3f(1, 1, 1);	//100% white
    glBegin(GL_LINES);{
        //Y axis
        glVertex3f(0, -150, 0);	// intentionally extended to -150 to 150, no big deal
        glVertex3f(0,  150, 0);
        
        //X axis
        glVertex3f(-150, 0, 0);
        glVertex3f( 150, 0, 0);
    }glEnd();
    
    //ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
    glutSwapBuffers();
}

void animate(){
    //codes for any changes in Camera
    
    //cameraAngle += cameraAngleDelta;	// camera will rotate at 0.002 radians per frame.	// keep the camera steady NOW!!
    
    //codes for any changes in Models
    
    rectAngle -= 1;
    
    //MISSING SOMETHING? -- YES: add the following
    glutPostRedisplay();	//this will call the display AGAIN
}

void keyboardListener(unsigned char key, int x,int y){
    switch(key){
        case '1':
            yaw(.05);
            break;
        case '2':
            yaw(-.05);
            break;
        case '3':
            roll(-.05);
            break;
        case '4':
            roll(.05);
            break;
        case '5':

            pitch(-.05);
            break;
        case '6':
            pitch(.05);
            break;
        case '7':	//increase rotation of camera by 10%
            roll(-.05);
            break;

        case '8':	//decrease rotation
            roll(.05);
            break;

        case '9':	//toggle grids
            canDrawGrid = 1 - canDrawGrid;
            break;
        case '=':
            walk_fly(1, true);
            break;
        case '-':
            walk_fly(-1, true);
            break;

        case 's':		//down arrow key
            walk_fly(-1, false);
            break;
        case 'w':		// up arrow key
            walk_fly(1, false);
            break;
        case 'c':
            render();
            break;
            
        default:
            break;
    }

}

void specialKeyListener(int key, int x,int y){
    switch(key){
        case GLUT_KEY_DOWN:		//down arrow key
            walk_fly(10, false);
            break;
        case GLUT_KEY_UP:		// up arrow key
            walk_fly(-10, false);
            break;
            
        case GLUT_KEY_RIGHT:
            starf(10);
            break;
        case GLUT_KEY_LEFT:
            starf(-10);
            break;
            
        case GLUT_KEY_PAGE_UP:
            walk_fly(10,true);
            break;
        case GLUT_KEY_PAGE_DOWN:
            walk_fly(-10, true);
            break;
            
        case GLUT_KEY_INSERT:
            break;
            
        case GLUT_KEY_HOME:
            break;
        case GLUT_KEY_END:
            break;
            
        default:
            break;
    }
}

void mouseListener(int button, int state, int x, int y){	//x, y is the x-y of the screen (2D)
    switch(button){
        case GLUT_LEFT_BUTTON:
            if(state == GLUT_DOWN){		// 2 times?? in ONE click? -- solution is checking DOWN or UP
                //drawaxes=1-drawaxes;
                render();
                break;

            }
        case GLUT_RIGHT_BUTTON:
            //........
            break;
            
        case GLUT_MIDDLE_BUTTON:
            //........
            break;
            
        default:
            break;
    }
}


void init(){
    //codes for initialization
    cameraAngle = 0;	// init the cameraAngle
    IDquadric = gluNewQuadric();
    cameraAngleDelta = 0.02;
    rectAngle = 0;
    height=20;
    canDrawGrid = true;
    r=20;
    
    a[0]=r*cos(cameraAngle);
    a[1]=r*sin(cameraAngle);
    a[2]=height;
    
    b[0]=b[1]=b[2]=0;
    
    c[0] = 0;
    c[1] = 0;
    c[2] = 1;
    
    //clear the screen
    glClearColor(BLACK, 0);

    //Add global ambient light
    lmodel_ambient[0] = .6;
    lmodel_ambient[1] = .6;
    lmodel_ambient[2] = .6;
    lmodel_ambient[3] = 1.0;
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

    //effect of local light source: point diffuse light
    /**/
    GLfloat diffusePoint[] = {1, 1, 1, 1.0}; //Color (0.5, 0.5, 0.5)

    for (int i=0; i<lightSources.size(); i++) {
        GLfloat position[] = {static_cast<GLfloat>(lightSources[i].x),static_cast<GLfloat>(lightSources[i].y), static_cast<GLfloat>(lightSources[i].z), 1.0}; //Positioned at (-10, -10, 5)
        if (i==0) {
            glLightfv(GL_LIGHT0, GL_DIFFUSE, diffusePoint); //
            glLightfv(GL_LIGHT0, GL_POSITION, position);
        }
        else if (i==1)
        {
            glLightfv(GL_LIGHT1, GL_DIFFUSE, diffusePoint); //
            glLightfv(GL_LIGHT1, GL_POSITION, position);
        }
        else if (i==2)
        {
            glLightfv(GL_LIGHT2, GL_DIFFUSE, diffusePoint); //
            glLightfv(GL_LIGHT2, GL_POSITION, position);
        }
        else if (i==3)
        {
            glLightfv(GL_LIGHT3, GL_DIFFUSE, diffusePoint); //
            glLightfv(GL_LIGHT3, GL_POSITION, position);
        }
        else if (i==4)
        {
            glLightfv(GL_LIGHT4, GL_DIFFUSE, diffusePoint); //
            glLightfv(GL_LIGHT4, GL_POSITION, position);
        }
        else if (i==5)
        {
            glLightfv(GL_LIGHT5, GL_DIFFUSE, diffusePoint); //
            glLightfv(GL_LIGHT5, GL_POSITION, position);
        }
        else if (i==6)
        {
            glLightfv(GL_LIGHT6, GL_DIFFUSE, diffusePoint); //
            glLightfv(GL_LIGHT6, GL_POSITION, position);
        }
        else if (i==7)
        {
            glLightfv(GL_LIGHT7, GL_DIFFUSE, diffusePoint); //
            glLightfv(GL_LIGHT7, GL_POSITION, position);
        }

    }

    glEnable(GL_NORMALIZE); //Automatically normalize normals needed by the illumination model
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0); //Enable light #0
    glEnable(GL_LIGHT1); //Enable light #1

    /************************
     / set-up projection here
     ************************/
    //load the PROJECTION matrix
    glMatrixMode(GL_PROJECTION);
    
    //initialize the matrix
    glLoadIdentity();
    
    //give PERSPECTIVE parameters
    gluPerspective(70, 1, 0.1, 10000.0);
    //field of view in the Y (vertically)
    //aspect ratio that determines the field of view in the X direction (horizontally)
    //near distance
    //far distance
}

int main(int argc, char **argv){
    readFile();

    glutInit(&argc,argv);
    glutInitWindowSize(WINDOWSIZE, WINDOWSIZE);
    glutInitWindowPosition(0, 0);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color
    
    glutCreateWindow("Offline: Ray Tracing");
    
    init();
    
    glEnable(GL_DEPTH_TEST);	//enable Depth Testing
    
    glutDisplayFunc(display);	//display callback function
    glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

    //ADD keyboard listeners:
    glutKeyboardFunc(keyboardListener);
    glutSpecialFunc(specialKeyListener);
    glutMouseFunc(mouseListener);

    
    glutMainLoop();		//The main loop of OpenGL
    
    return 0;
}