//
//  vector.h
//  RayTracing
//
//  Created by Bashima Islam on 12/10/15.
//  Copyright © 2015 Bashima Islam. All rights reserved.
//

double sqring(double x)
{
    return(x * x);
}

class Vector
{
public:
	double x,y,z;
	Vector (){
	}
	Vector (double a, double b, double c){
		x = a;
		y = b;
		z = c;
	}
    double getLength()
    {
        double length = sqrt(sqring(x) + sqring(y) + sqring(z));
        return length;
    }
	void normalize(){
        double length = getLength();
        if (length == 0.0f) return;
		x = x / length;
		y = y / length;
		z = z / length;
	}

};


Vector operator+ (Vector v, Vector u)
{
	Vector res;
	res.x = v.x + u.x;
	res.y = v.y + u.y;
	res.z = v.z + u.z;
	return res;
}

Vector operator- (Vector v, Vector u)
{
	Vector res;
	res.x = v.x - u.x;
	res.y = v.y - u.y;
	res.z = v.z - u.z;
	return res;
}

Vector operator* (Vector v, double r)
{
	Vector res;
	res.x = v.x*r;
	res.y = v.y*r;
	res.z = v.z*r;
	return res;
}

Vector operator* (double r, Vector v)
{
	Vector res;
	res.x = v.x * r;
	res.y = v.y * r;
	res.z = v.z * r;
	return res;
}

Vector static cross(Vector u, Vector v)
{
	Vector res;
	res.x = u.y * v.z - u.z * v.y;
	res.y = u.z * v.x - u.x * v.z;
	res.z = u.x * v.y - u.y * v.x;
	return res;
}

double operator* (Vector v, Vector u)
{
	return v.x*u.x+v.y*u.y+v.z*u.z;
}
